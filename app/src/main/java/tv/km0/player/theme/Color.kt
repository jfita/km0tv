/*
 * km0.tv
 * Copyright (C) 2021 jordi fita i mas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package tv.km0.player.theme

import androidx.compose.ui.graphics.Color

val Purple300 = Color(0xFFCD52FC)
val Purple600 = Color(0xFF9F00F4)
val Purple700 = Color(0xFF8100EF)
val Purple800 = Color(0xFF0000E1)

val Red300 = Color(0xFFD00036)
val Red800 = Color(0xFFEA6D7E)

val Gray100 = Color(0xFFF5F5F5)
val Gray900 = Color(0xFF212121)
